# Overview

This is the Primary Root Repository for Consumer Edge Platform (Anthos Retail Edge). This repository purpose is to provide a working example of a Primary Root Repository.

This repository is intended to be used by a Platform Engineer or Operations Engineer. Other software desired are delegated out via (Cluster Trait Repositories)[https://gitlab.com/gcp-solutions-public/retail-edge/available-cluster-traits] or customer software via `RepoSync` or `RootSync` configurations set within this repository.

## Repo Model / Types

### Primary Root Repository (RootSync)

The purpose of a Primary Root Repository is to hold/contain the skeleton and primary configurations for all associated clusters. The structure of this repostiory is designed to promote high-scale (many clusters).

### Root Repository (RootSync)
The purpose of the `/shared` (also called "embedded") repo-sync is to create a homogenous cluster configuration. All of the configurations are applied to all cluters and therefore have a wide sphere of change.

## Using this Repository

This repoistory is installed per each cluster during the provisioning cluster process. This is the only imperative command during the provisioning phase post cluster creation.  Simply apply the following YAML and `kubectl apply -f <file>` to apply.

```yaml
# NOTE: In this example, the name of the cluster is `con-edge-cluster` and matches the folder /config/clusters/con-edge-cluster
# root-sync.yaml
apiVersion: configsync.gke.io/v1beta1
kind: RootSync
metadata:
  name: "primary-root-sync"
  namespace: config-management-system
spec:
  sourceFormat: "unstructured"
  git:
    repo: "https://gitlab.com/gcp-solutions-public/retail-edge/primary-root-repo-template.git"
    branch: "main"
    period: "60s"                                       # check for changes every minute
    dir: "/config/clusters/con-edge-cluster/meta"       # con-edge-cluster is the default Cluster name
    auth: "token"
    secretRef:
      name: "git-creds"
```

## CI/CD Pipeline Verification

Recommened production use of Primary Root Repository is to follow standard development best practices by reviewing changes through the Merge Request pattern (ie: introduce change on a git-branch, run CI/CD, issue a Merge Request, Review, mere to `main`).

### GitLab (preferred)
In this project, the `.gitlab-ci.yml` file is a demonstration of a basic CI/CD pipeline. The pipeline uses `nomos vet` to verify the configuration of all clusters. The sample pipeline is setup to run without reaching out to a cluster. It is possible, but not enabled, to provide KUBECONFIG contexts for all clusters, then checks would verify CRDs and full `nomos` checking.

## Local Validation

Assuming `nomos` is installed (via `gcloud components install nomos`)

```
# Check config for con-edge-cluster (default cluster name)
nomos vet --source-format=unstructured --no-api-server-check --path config/clusters/con-edge-cluster
```
