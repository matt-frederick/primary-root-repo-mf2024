#!/bin/bash

# Get list of all cluster names
declare -a CLUSTERS=$(ls -l config/clusters | grep ^d | awk '{print $9}')

ERROR_FOUND=false
# Loop through all clusters and run nomos vet
for cluster in $CLUSTERS; do
    nomos vet --source-format=unstructured --no-api-server-check --path config/clusters/$cluster
    if [[ $? -ne 0 ]]; then
        ERROR_FOUND=true
    fi
done

if [[ $ERROR_FOUND == true ]]; then
    echo "Nomos validation failed"
    exit 1
fi